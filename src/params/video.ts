import type { ParamMatcher } from '@sveltejs/kit';

export const match: ParamMatcher = (param) => {
	return ['mente', 'tecnologia'].indexOf(param) !== -1;
};