import type { ParamMatcher } from '@sveltejs/kit';

export const match: ParamMatcher = (param) => {
	return ['main', 'mente', 'tecnologia'].indexOf(param) !== -1;
};