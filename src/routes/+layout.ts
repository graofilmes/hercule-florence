export const load = ({ url }) => {
	const { pathname } = url;
  const routeKey = ['/main', '/mente', '/tecnologia'].indexOf(pathname) !== -1
                  ? '/main' : pathname

	return {
		routeKey
	};
};