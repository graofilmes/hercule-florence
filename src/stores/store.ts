import { writable } from "svelte/store";

export const audioDescription = writable(false);
export const libras = writable(false);
export const hideMenu = writable(false);
export const isSliding = writable(false);
