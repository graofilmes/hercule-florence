1
00:00:17,750 --> 00:00:19,708
<b>Após muitas viagens</b>
<b>e destinos,</b>

2
00:00:19,833 --> 00:00:23,125
<b>não faz muito tempo, cheguei</b>
<b>nesta Vila de São Carlos...</b>

3
00:00:23,125 --> 00:00:26,500
<b>... uma campina, distante</b>
<b>do centro do mundo.</b>

4
00:00:27,500 --> 00:00:30,541
<b>Através de minha janela,</b>
<b>na Rua das Flores,</b>

5
00:00:30,750 --> 00:00:34,583
<b>percebo que a luz faz desbotar</b>
<b>minha camisa indiana sobre a cama,</b>

6
00:00:34,833 --> 00:00:38,083
<b>ao mesmo tempo que</b>
<b>me revela suas cores.</b>

7
00:00:39,083 --> 00:00:42,166
<b>Por tal capacidade de</b>
<b>colorir e descolorir,</b>


8
00:00:42,416 --> 00:00:44,833
<b>acredito que um dia,</b>
<b>no futuro,</b>


9
00:00:45,041 --> 00:00:48,166
<b>poderemos imprimir</b>
<b>pela ação direta da luz.</b>


10
00:00:48,500 --> 00:00:50,083
<b>Não seria maravilhoso?!</b>

11
00:00:53,875 --> 00:00:57,375
<b>Preciso imediatamente</b>
<b>estudar mais sobre química;</b>

12
00:00:57,708 --> 00:01:03,833
<b>conhecer outras substâncias que,</b>
<b>numa folha de papel, reajam à luminosidade.</b>

13
00:01:05,250 --> 00:01:09,375
<b>O nitrato de prata tem essa virtude,</b>
<b>de escurecer ao sol.</b>

14
00:01:10,708 --> 00:01:13,500
<b>Se fosse um pouco mais fácil</b>
<b>consegui-lo aqui,</b>

15
00:01:13,750 --> 00:01:16,875
<b>poderia fazer experimentos</b>
<b>com maior frequência.</b>

16
00:01:18,500 --> 00:01:21,625
<b>Preciso de uma caixa escura</b>
<b>para servir de câmera…</b>

17
00:01:21,791 --> 00:01:24,291
<b>com uma lente de óculos</b>
<b>e uma pequena entrada</b>

18
00:01:24,291 --> 00:01:27,833
<b>para deixar passar somente</b>
<b>uma quantidade limitada de luz.</b>

19
00:01:29,208 --> 00:01:32,791
<b>O que tenho à mão, me permite</b>
<b>construir algo bem rudimentar,</b>

20
00:01:32,916 --> 00:01:36,250
<b>que depois, claro,</b>
<b>poderá ser aperfeiçoado.</b>

21
00:01:39,875 --> 00:01:44,500
<b>A ação da luz desenha no</b>
<b>papel sensível o busto de La Fayette.</b>

22
00:01:44,666 --> 00:01:48,666
<b>Os claros tornaram-se escuros</b>
<b>e vice-versa. </b>

23
00:01:49,666 --> 00:01:50,583
<b>Fora da câmera, </b>

24
00:01:50,666 --> 00:01:54,625
<b>o papel escurece completamente</b>
<b>e a memória se perde.</b>

25
00:01:55,708 --> 00:01:56,916
<b>Quem diria…</b>

26
00:01:57,041 --> 00:02:02,291
<b>é ainda mais difícil fixar uma imagem</b>
<b>do que minhas próprias inquietações.</b>

27
00:02:08,041 --> 00:02:10,291
<b>Fabuloso este meio</b>
<b>de obter desenhos</b>

28
00:02:10,291 --> 00:02:12,541
<b>e reproduzir</b>
<b>qualquer paisagem,</b>

29
00:02:12,666 --> 00:02:14,208
<b>não pela mão do homem, </b>

30
00:02:14,291 --> 00:02:15,833
<b>mas pela 'escrita da luz'…</b>

31
00:02:17,208 --> 00:02:19,666
<b>Fabulosa esta</b>
<b>fotografia!</b>

