1
00:00:13,166 --> 00:00:15,291
<b>Construí uma primeira</b>
<b>câmara escura</b>

2
00:00:15,333 --> 00:00:17,458
<b>utilizando uma pequena caixa;</b>

3
00:00:17,625 --> 00:00:20,125
<b>e a cobri com minha paleta de pintura.</b>

4
00:00:21,333 --> 00:00:23,541
<b>No orifício coloquei uma lente. </b>

5
00:00:23,875 --> 00:00:27,416
<b>Enfiei um espelho na caixa e,</b>
<b>a uma altura adequada,</b>

6
00:00:27,500 --> 00:00:30,208
<b>introduzi um pedaço</b>
<b> de papel embebido</b>

7
00:00:30,291 --> 00:00:33,000
<b>em uma solução fraca</b>
<b>de nitrato de prata.</b>

8
00:00:39,666 --> 00:00:42,833
<b>A segunda câmara conta</b>
<b>com um tubo horizontal,</b>

9
00:00:42,875 --> 00:00:47,500
<b>ao qual é adaptado outro,</b>
<b>com a lente, que pode ser regulada.</b>

10
00:00:50,458 --> 00:00:53,041
<b>Acima do tubo,</b>
<b>há um pequeno orifício</b>

11
00:00:53,083 --> 00:00:55,166
<b>que deve ser mantido</b>
<b>sempre fechado</b>

12
00:00:55,208 --> 00:00:58,000
<b>e que serve para</b>
<b>observar a imagem.</b>

13
00:00:58,750 --> 00:01:03,125
<b>Cada câmara escura seria um pintor</b>
<b>que teríamos às ordens.</b>

14
00:01:03,916 --> 00:01:06,791
<b>E como será útil para os retratos!</b>

15
00:01:07,208 --> 00:01:10,125
<b>Isto mesmo: a imagem</b>
<b>de uma pessoa</b>

16
00:01:10,166 --> 00:01:15,333
<b>será apreendida e fixada no papel</b>
<b>por uma simples ação química.</b>

17
00:01:16,916 --> 00:01:21,041
<b>Coloquei esses aparelhos</b>
<b>numa sala naturalmente escura.</b>


18
00:01:21,083 --> 00:01:22,583
<b>O objeto era uma janela: </b>

19
00:01:23,541 --> 00:01:29,166
<b>viam-se os caixilhos, o telhado</b>
<b>de uma casa em frente e parte do céu. </b>

20
00:01:29,416 --> 00:01:32,375
<b>Deixei assim durante 4 horas:</b>

21
00:01:33,000 --> 00:01:36,291
<b>e quando fui verificar,</b>
<b>após retirar o papel,</b>

22
00:01:36,458 --> 00:01:40,708
<b>encontrei lá a janela </b>
<b>representada de uma maneira fixa.</b>

23
00:01:41,791 --> 00:01:44,833
<b>Não terei eu iniciado</b>
<b>a arte mais do que maravilhosa</b>

24
00:01:44,916 --> 00:01:49,458
<b>de desenhar qualquer objeto,</b>
<b>de captar uma paisagem qualquer,</b>

25
00:01:49,625 --> 00:01:52,916
<b>sem o trabalho de o fazer</b>
<b>com a própria mão? </b>

26
00:01:53,708 --> 00:01:57,000
<b>Neste ponto,</b>
<b>a inteligência vai descansar</b>

27
00:01:57,041 --> 00:01:59,291
<b>e pairar sobre novos tesouros,</b>

28
00:01:59,333 --> 00:02:02,125
<b>sobre novas fontes de felicidade!</b>

