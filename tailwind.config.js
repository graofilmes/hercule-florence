/** @type {import('tailwindcss').Config} */
export default {
  content: ['./src/**/*.{html,js,svelte,ts}'],
  theme: {
    extend: {
      colors: {
        theme: {
          black: '#040707',
          grey: '#636466',
          blue: '#00224b',
          lightblue: '#84929f',
          darkblue: '#232b37',
          green: '#6a7b59',
          lime: '#c0c174',
          yellow: '#d6a411',
          stone: '#aca392',
          brown: '#ad8251',
          terracota: '#815250',
          pink: '#b88c9e',
          red: '#a02308',
        }
      }
    }
  },
  plugins: []
};